FROM ubuntu:20.04
ENV REFRESHED_AT 2022-04-27_17:00

RUN apt-get -qq update

RUN apt-get update \
    && apt-get install -y wget gdb make \
    && rm -rf /var/lib/apt/lists/*

# Install cmake
RUN wget https://github.com/Kitware/CMake/releases/download/v3.23.1/cmake-3.23.1-Linux-x86_64.sh \
    -q -O /tmp/cmake-install.sh \
    && chmod u+x /tmp/cmake-install.sh \
    && mkdir /usr/bin/cmake \
    && /tmp/cmake-install.sh --skip-license --prefix=/usr/bin/cmake \
    && rm /tmp/cmake-install.sh

ENV PATH="/usr/bin/cmake/bin:${PATH}"

# Install GCC for ARM.
WORKDIR /home/dev

# Pull the gcc-arm-none-eabi tarball
RUN wget https://developer.arm.com/-/media/Files/downloads/gnu-rm/10.3-2021.10/gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2 \
    && tar xvf gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2 \
    && rm gcc-arm-none-eabi-10.3-2021.10-x86_64-linux.tar.bz2 && ls

# Set up the compiler path.
ENV PATH="/home/dev/gcc-arm-none-eabi-10.3-2021.10/bin:${PATH}"

WORKDIR /usr/project

CMD ["arm-none-eabi-gcc", "--version"]